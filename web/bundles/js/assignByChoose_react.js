var CLASS_ID = null;
var SEMESTER_ID = null;
var URL = '/subject/assign_list?class_id=CLASS_ID&semester_id=SEMESTER_ID id=form-assign';
var App = React.createClass({
    getInitialState: function() {
        return {

            text: '',
            assignSubjects: [],
            classes: [],
            subjects: [],
            semester: [],
            resetSubjects: [],
            resetSemester: [],

        }

    },

    render: function () {
        return(
                <div>
                    <ClassesList classes={this.state.classes} onToAdd={this.handleToAdd} onLoadClass={this.handleClassesAdd} onResetSubject={this.replaceAssSubj} />
                    <SemesterList CLASS_ID={this.state.CLASS_ID} semester={this.state.semester} onToAdd={this.handleToAdd} onLoadSemester={this.handleSemesterAdd}
                                  assignSubjects={this.state.assignSubjects} onResetSubject={this.replaceAssSubj} onResetSemester={this.replaceSemester}/>
                    <TodoForm onToAdd={this.handleToAdd} handleClassChange={this.handleToAdd} onLoadSubject={this.handleSubjectAdd} subjects={this.state.subjects}/>
                    <AssignSubjectList assignSubjects={this.state.assignSubjects} deleteTodo={this.handleTodoDelete} />
                </div>
        )
    },
    replaceAssSubj: function () {
        var newState = this.state.resetSubjects.map(function(obj) {
            return {id: obj.id, name: obj.name};
        })
        this.setState({assignSubjects: newState});
    },
    replaceSemester: function () {
        var newState = this.state.resetSemester.map(function (obj) {
            return {id: obj.id, name: obj.name};

        })
        this.setState({semester: newState});
    },
    getLength(state, id) {
        var result = state.filter((index) => {
            return index.id === parseInt(id);
        });
        return result;

    },
    handleToAdd: function (id, text) {
        var check = this.getLength(this.state.assignSubjects, id);
        if(!check.length) {
            var newAssSunbj = {
                id: id,
                name: text
            }
            this.setState({assignSubjects:this.state.assignSubjects.concat(newAssSunbj)});
            //console.log(this.state.assignSubjects);
        }

    },
    handleClassesAdd: function (id, text) {
        var newClassesToAdd = {
            id: id,
            name: text
        }
        this.setState({classes: this.state.classes.concat(newClassesToAdd)});
    },
    handleSemesterAdd: function (id, text) {
        var newSemesterToAdd = {
            id: id,
            name: text
        }
        this.setState({semester: this.state.semester.concat(newSemesterToAdd)});
    },
    handleSubjectAdd: function (id, text) {
        var newSubjectsToAdd = {
            id: id,
            name: text
        }
        this.setState({subjects: this.state.subjects.concat(newSubjectsToAdd)});
    },
    handleTodoDelete: function (element) {
        var assignSubjects = this.state.assignSubjects;
        for(var i = 0; i < assignSubjects.length; i++) {
            if(assignSubjects[i].id == element.id) {
                assignSubjects.splice(i, 1);
            }
        }
        this.setState({assignSubjects: assignSubjects});
    }
});

var TodoForm = React.createClass({
    getInitialState:function(){
        return {selectValue:''};
    },
    render: function () {
        return(

                <div>
                    <div className="form-group">
                        <select ref="key" onChange={this.onChange} >
                            <option value="0">Choose Subject</option>
                            {
                                this.props.subjects.map(subjectlist => {
                                    return (
                                        <option subjectlist={subjectlist} key={subjectlist.id} value={subjectlist.id} >{subjectlist.name}</option>
                                    )
                                })
                            }
                        </select>
                        <a className="collection-add btn btn-default" title="Add subject" onClick={this.onSubmit} ><span className="glyphicon glyphicon-plus-sign"></span></a>
                    </div>
                </div>

        )
    },

    componentDidMount() {
        var id = 0;
        var form = $('#form-assign');
       // var url = form.attr('action').replace('CLASS_ID', id);
        var url = URL.replace('CLASS_ID', id);
        var data = form.serialize();
        var props = this.props;
        $.post(url, data, function(result) {
            for(var i=0; i< result.subject.length; i++){
                props.onLoadSubject(result.subject[i].id,result.subject[i].name);
            }
        });
    },

    onChange: function (e) {
        var index = e.target.selectedIndex;
        this.setState({selectValue:e.target[index].text});

    },

    onSubmit: function(e) {
        e.preventDefault();
        var id = parseInt(this.refs.key.value);
        var text = this.state.selectValue;
        if(!text){
            alert('Please select a subject! ');
        }
        else {
            this.props.onToAdd(id, text);
        }

    }
});

var AssignSubjectList = React.createClass({
    getInitialState: function() {
        return {
            currentSubjects: []
        }
    },
    render0: function () {
        return(

            <table className="table table-striped table-hover" id="assign_subjects">

                <tr>
                    <th>Subjects</th>
                </tr>

                {
                    this.state.currentSubjects.map(subject => {
                        return (
                            <tbody><tr todo={subject} key={subject.id} id={subject.id}><td>
                                <input type="text"  value={subject.name} data-id={subject.id} readOnly />
                                <input type="hidden" value={subject.id} name="form[subject_id][]" />
                                <a className="collection-remove btn btn-default" title="Delete" href="#" onClick={this.onDelete.bind(this, subject)}>
                                    <span className="glyphicon glyphicon-trash"></span>
                                </a>
                            </td></tr> </tbody>
                        )
                    })
                }

            </table>
        )
    },
    render: function () {
        return(

            <table className="table table-striped table-hover" id="assign_subjects">
                <tr>
                    <th>Subjects</th>
                </tr>
                <tbody>
                {
                    this.props.assignSubjects.map(subject => {
                        return (
                            <tr subject={subject} key={subject.id} id={subject.id}><td>
                                <input type="text"  value={subject.name} data-id={subject.id} readOnly />
                                <input type="hidden" value={subject.id} name="form[subject_id][]" />
                                <a className="collection-remove btn btn-default" title="Delete" href="#" onClick={this.onDelete.bind(this, subject)}>
                                    <span className="glyphicon glyphicon-trash"></span>
                                </a>
                            </td></tr>)

                    })
                }
                </tbody>
            </table>
        )
    },
    onDelete: function (subject) {
        this.props.deleteTodo(subject);
    }

});

var ClassesList = React.createClass({
    getInitialState: function() {

        return {
            clId: 0
        }
    },
    handleClassFilter: function(e) {
        this.setState({clId: sessionStorage.getItem('classId')});
        CLASS_ID = this.state.clId;

    },
    render: function () {
        let classFiltered =  this.props.classes.filter((classes)=> {
                return classes.id == this.state.clId;
            }
        );
        return(
            <select onClick={this.handleClassFilter} onChange={this.handleClassChange} ref="id" id="form_class_id" name="form[class_id]" required="required" >
                {
                    classFiltered.map(classlist => {
                        return (
                            <option classlist={classlist} key={classlist.id} value={classlist.id} >{classlist.name}</option>
                        )
                    })
                }

            </select>

        )
    },
    componentDidMount() {
        this.timer = setInterval(this.handleClassFilter, 100);
        var id = 0;
        var form = $('#form-assign');
       // var url = form.attr('action').replace('CLASS_ID', id);
        var url = URL.replace('CLASS_ID', id);
        var data = form.serialize();
        var props = this.props;
        $.post(url, data, function(result) {
            for(var i=0; i< result.classes.length; i++){
                props.onLoadClass(result.classes[i].id,result.classes[i].name);
            }
        });
    },
    componentWillUnmount: function(){
        clearInterval(this.timer);
    },
    handleClassChange: function(e) {
        e.preventDefault();
        var id = this.refs.id.value;
        CLASS_ID = id;
        var form = $('#form-assign');
      //  var url = form.attr('action').replace('CLASS_ID',  CLASS_ID);
        var url = URL.replace('CLASS_ID',  CLASS_ID);
        url = url.replace('SEMESTER_ID', SEMESTER_ID);
        var data = form.serialize();
        var props = this.props;
        props.onResetSubject();
        $.post(url, data, function(result) {

           for(var i=0; i< result.subject_assign.length; i++){
                props.onToAdd(result.subject_assign[i].id, result.subject_assign[i].name);
           }
        });
    }
});

var SemesterList = React.createClass({

    render: function () {
        return (
            <select onChange={ this.handleSemesterChange } ref="id" id="form_semester"
                    name="form[semester_id]" required="required">
                <option value="">Choose Semester</option>
                {
                    this.props.semester.map(semesterlist => {
                        return (
                            <option semesterlist={semesterlist} key={semesterlist.id}
                                    value={semesterlist.id}>{semesterlist.name}</option>
                        )
                    })
                }

            </select>
        )
    },
    componentDidMount() {
        var element = document.getElementsByClassName('assignElement');
        for (var i = 0; i < element.length; i++) {
            element[i].addEventListener('click', this.handleBodyClick, false);
        }
       // document.getElementById('assignS').addEventListener('click', this.handleBodyClick, true);

    },
    componentWillUnmount() {
        // make sure you remove the listener when the component is destroyed
        document.getElementById('assignS').removeEventListener('click', this.handleBodyClick, false);
    },
    handleClick: function (e) {
        e.preventPropagation();
    },
    handleBodyClick() {
        this.props.onResetSemester();
        this.props.onResetSubject();
        this.handleLoadSemester();

    },
    handleLoadSemester: function () {
        var props = this.props;
        var form = $('#form-assign');
        //var url = form.attr('action').replace('CLASS_ID',  CLASS_ID);
        var url = URL.replace('CLASS_ID',  CLASS_ID);
        var data = form.serialize();
        $.post(url, data, function (result) {
            for (var i = 0; i < result.semester.length; i++) {
                props.onLoadSemester(result.semester[i].id, result.semester[i].name);
            }
        });
    },
    handleSemesterChange: function (e) {
        e.preventDefault();
        var id = this.refs.id.value;
        SEMESTER_ID = id;
        var form = $('#form-assign');
        // var url = form.attr('action').replace('CLASS_ID',  CLASS_ID);
        var url = URL.replace('CLASS_ID',  CLASS_ID);
        url = url.replace('SEMESTER_ID', SEMESTER_ID);
        var data = form.serialize();
        var props = this.props;
        props.onResetSubject();
        $.post(url, data, function (result) {
            for (var i = 0; i < result.subject_assign.length; i++) {
                props.onToAdd(result.subject_assign[i].id, result.subject_assign[i].name);
            }
        });
          document.removeEventListener('click', this.hangleClick, false);
    },
    deleteRow() {
        $("#assign_subjects").find("tr:not(:first)").remove();
    }
});
/*
document.getElementById('myBtn').click(function() {
    var f = sessionStorage.getItem('classId');
    alert(f);
});
document.addEventListener('click', kufa(1));
function kufa(classId) {

    sessionStorage.setItem('classId', classId);
    var s = document.getElementById("babel_script");
    s.src = "/bundles/js/assignByChoose_react.js";
    modal.style.display = "block";

}*/
ReactDOM.render(
    <App/>,
    document.getElementById('app')
);