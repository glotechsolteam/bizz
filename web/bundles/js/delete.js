$(document).ready(function(){
  $('.progress').hide();
  $('.btn-danger').click(function(e) {
     e.preventDefault();

     var row = $(this).parents('tr');

     var id = row.data('id');

     //alert(id);
     
     var form = $('#form-delete')

     var url = form.attr('action').replace('ID', id);

     var data = form.serialize();

    //alert(url);
    var res = confirm('Do you realy want delete this item');
    if(res) {
        $('.ajax_loader_main').show();
        $.post(url, data, function(result) {
            if(result.removed==1) {
                $('.ajax_loader_main').hide();
                row.remove();
            }
        });
    }

  });  

});
