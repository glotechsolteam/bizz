<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadAdminUsers implements FixtureInterface, ContainerAwareInterface
{
    /**
     * 
     * @var ContainerAwareInterface
     */
    private $container;
    
    /**
     * Set the ContainerAwareInteface method to load any services for data fixture classes
     * 
     * {@inheritDoc}
     * @see \Symfony\Component\DependencyInjection\ContainerAwareInterface::setContainer()
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    /**
     * Main method to load data into database
     * {@inheritDoc}
     * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
     */
    public function load(ObjectManager $manager)
    {
        //Add admin user
        $userAdmin = new User();
        $userAdmin->setUsername('admin');
        
        $encoder  = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($userAdmin, 'test');
        
        $userAdmin->setPassword($password);
        $userAdmin->setEmail('admin@example.com');
        $userAdmin->setProfilePicture('default-user.png');
        $userAdmin->setRoles(['ROLE_ADMIN']);
        $userAdmin->setRoles(['ROLE_CREATOR']);
        $userAdmin->isactive = '1';

        $manager->persist($userAdmin);
        $manager->flush();
    }
}
