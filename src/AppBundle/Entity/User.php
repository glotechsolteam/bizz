<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * User
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 */
class User implements AdvancedUserInterface {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var array
     *
     * @ORM\Column(type="json_array", nullable= true)
     */
    private $roles = [];

    /**
     * @ORM\Column(name="isactive", type="boolean")
     */
    public  $isactive;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username) {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername() {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password) {
        if (!is_null($password)) {
            $this->password = $password;
        }
       // $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    public function eraseCredentials() {
        
    }


    public function getRoles() {
        $tmpRoles = $this->roles;
        if (in_array('USER_ROLE', $tmpRoles) === false) {
            $tmpRoles[] = 'USER_ROLE';
        }


        return $tmpRoles;
    }

    public function setRoles($roles) {
        $this->roles = $roles;
    }

    public function getSalt() {
        
    }

    public function isAccountNonExpired(): bool {
        return true;
    }

    public function isAccountNonLocked(): bool {
        return true;
    }

    public function isCredentialsNonExpired(): bool {
        return true;
    }

    public function isActive() {
        return $this->isactive;
    }


    public function isEnabled() {
        return $this->isactive;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="profile_picture", type="string", length=255)
     */
    private $profile_picture;
    private $file;

    public function getFile() {
        return $this->file;
    }

    public function setFile($file) {
        return $this->file = $file;
    }
     /**
     * Set profile_picture
     *
     * @param string $profile_picture
     *
     * @return User
     */
    public function setProfilePicture($profile_picture)
    {
        $this->profile_picture = $profile_picture;
        return $this;
    }

    /**
     * Get profile_picture
     *
     * @param string $profile_picture
     *
     * @return User
     */
    public function getProfilePicture()
    {
        return $this->profile_picture;
    }

    public function getUploadDir() {
        return 'upload/images';
    }
    public function getAbsolutRoot() {
        return $this->getUploadRoot();
    }
    public function getWebPath() {
        return $this->getUploadDir().'/'.$this->profile_picture;
    }
    public function getUploadRoot() {
        return __DIR__.'/../../../web/'.$this->getUploadDir().'/';

    }
    public function upload() {
        if ($this->file === null) {
            return;
        }

        $this->profile_picture = $this->file->getClientOriginalName();
        if (!is_dir($this->getUploadRoot())) {

            //mkdir($this->getUploadRoot(), '0777', true);
        }
       
        $this->file->move($this->getUploadRoot(), $this->profile_picture);
        unset($this->file);
    }
}
