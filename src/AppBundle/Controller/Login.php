<?php
/**
 * Created by PhpStorm.
 * User: kufa
 * Date: 30.01.18
 * Time: 11:36
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class Login extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authUtils)
    {
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findAll();

        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error
        ));
    }

    /**
     * @Route("/userlist"){
     *
     */
    public function userlistAction() {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository(User::class)->findAll();
        return $this->render('login/userlist.html.twig', [
            'users'=>$users
        ]);
        
       
        
    }

     /**
     * @Route("/userlist_pdf"){
     *
     */
    public function userlistPDFAction() {
        $pdf = new PDFController();
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository(User::class)->findAll();
                $html = $this->render('login/userlist.html.twig', [
            'users' => $users
        ]); 
         $pdf->returnPDFResponseFromHTML($html, $this);
       
    }
    /**
     * @Route("/edituser/{id}", name="user.edit")
     * @ParamConverter("user", class="AppBundle\Entity\User")
     * @param User user
     * @return Response
     */
    public function editUserAction(Request $request, User $user, UserPasswordEncoderInterface $passwordEncoder, \Swift_Mailer $mailer) {

        $form_profile_yesPass = $this->createFormBuilder($user)
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class
            ])           
            ->add('submit', SubmitType::class, array('label'=>'Save'))
            ->getForm();

        $form_profile_noPass = $this->createFormBuilder($user)
            ->add('email', EmailType::class)
            ->add('username', TextType::class)
            ->add('isactive', ChoiceType::class, array(
                'expanded' => true,
                'choices'  => array(
                    'Yes' => true,
                    'No' => false,
                ),))
            ->add('roles', ChoiceType::class, [
                'multiple' => true,
                'expanded' => true,
                'choices' => [
                    'Creator' => 'ROLE_CREATOR',
                    'Admin' => 'ROLE_ADMIN',
                ]
            ])
            ->add('file', FileType::class, array(
            'required'   => false,
            'label'      => 'ProfilePicture'
        ))
            // ->add('profile_picture', TextType::class)
            ->add('submit', SubmitType::class, array('label'=>'Save'))
            ->getForm();
        
        $em = $this->getDoctrine()->getManager();
       // $form = $this->createForm(UserType::class, $user);

        $form_profile_yesPass->handleRequest($request);
        if ($form_profile_yesPass->isSubmitted() && $form_profile_yesPass->isValid()) {
            // 3) Encode the password (you could also do this via Doctrine listener)            
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $em->clear();
            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            return $this->redirect('../userlist');
        }
        $form_profile_noPass->handleRequest($request);
        if ($form_profile_noPass->isSubmitted() && $form_profile_noPass->isValid()) {
            // 3) Encode the password (you could also do this via Doctrine listener)
            $em = $this->getDoctrine()->getManager();
            $user->upload();
          //  $user->setPassword($password);
           // $user->setEmail($form_profile_noPass->getData();
            //$em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $em->clear();
            $this->sendMail($user->getUsername(),$user->getEmail(), $mailer);
            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            return $this->redirect('../userlist');
        }
        return $this->render('login/editUser.html.twig', [
            'user' => $user,
            'img_link' => $user->getProfilePicture(),
            'form_profile_noPass' => $form_profile_noPass->createView(),
            'form_profile_yesPass' => $form_profile_yesPass->createView()
        ]);
    }
    public function sendMail($name, $emailTo, $mailer) {
         $message = (new \Swift_Message('Hello Email'))
        ->setFrom('office@glotechsol.com')
        ->setTo($emailTo)
        ->setBody(
            'Dear '.$name.  
            ' your profile is updated'
        )
        /*
         * If you also want to include a plaintext version of the message
        ->addPart(
            $this->renderView(
                'Emails/registration.txt.twig',
                array('name' => $name)
            ),
            'text/plain'
        )
        */
    ;

    $mailer->send($message);
    }
}
