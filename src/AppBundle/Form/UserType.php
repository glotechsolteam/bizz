<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Tests\Fixtures\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class UserType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('email', EmailType::class)
                ->add('username', TextType::class)
                ->add('password', RepeatedType::class, [
                    'type' => PasswordType::class
                ])
                ->add('isactive', ChoiceType::class, array(
                     'expanded' => true,
                     'choices'  => array(
                     'Yes' => true,
                     'No' => false,
                     ),))
                ->add('roles', ChoiceType::class, [
                    'multiple' => true,
                    'expanded' => true,
                    'choices' => [
                        'Creator' => 'ROLE_CREATOR',
                        'Admin' => 'ROLE_ADMIN',
                    ]
                ])
                ->add('file', FileType::class,  array(
                    'required'   => false,
                    'label'      => 'ProfilePicture'
                ))
               // ->add('profile_picture', TextType::class)
                ->add('submit', SubmitType::class, array('label'=>'Save'))
        ;    
    }

    public function configureOptions(OptionsResolver $resolver)
    {
   //     $resolver->setDefaults(array(
   //         'data_class' => User::class,
   //     ));
    }
}
